<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Buku extends Model
{
  protected $table = 'tb_buku';
  protected $primaryKey = 'buku_id';      
}
