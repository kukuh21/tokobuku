<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BuatTabelBuku extends Migration
{
    public function up()
    {
      Schema::create('tb_buku', function(Blueprint $table){
          $table->increments('buku_id');
          $table->string('buku_kode');
          $table->integer('kategori_id')->unsigned();
          $table->string('buku_nama', 255);
          $table->integer('pengarang_id');
          $table->bigInteger('buku_harga')->unsigned();
          $table->integer('buku_stok')->unsigned();
          $table->integer('penerbit_id');
          $table->timestamps();
      });
    }

    public function down()
    {
        Schema::drop('tb_buku');
    }
}
