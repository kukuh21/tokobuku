<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BuatTabelPenerbit extends Migration
{
    public function up()
    {
      Schema::create('tb_penerbit', function(Blueprint $table){
          $table->increments('penerbit_id');
          $table->string('penerbit_kode', 255);
          $table->string('penerbit_nama', 255);
          $table->integer('kota_id');
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tb_penerbit');
    }
}
