<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use App\Kota;
use App\Negara;
use JsValidator;
use Illuminate\Validation\Rule;
use DB;

class KotaController extends Controller
{
  public function index()
  {
    return view('kota.kota_index');
  }

  public function listData()
  {
    // $kota = Kota::all();
    // $kota = Kota::with('negara');
    $kota = DB::table('tb_kota')
        ->leftJoin('tb_negara', 'tb_kota.negara_id', '=', 'tb_negara.negara_id')
        ->get();
    $no = 0;
    $data = array();
    foreach($kota as $list){
      $no ++;
      $row = array();
      $row[] = $no;
      $row[] = 'KTA0'.$list->kota_id;
      $row[] = $list->kota_nama;
      $row[] = $list->negara_nama;
      $row[] = '<div class="btn-group">
              <a href="' . route('kota.edit', $list->kota_id) . '" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
              <a onclick="deleteData('.$list->kota_id.')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a></div>';
      $data[] = $row;
    }
    $output = array("data" => $data);
    return response()->json($output);
  }

  public function rulesCreate()
  {
    $rules = [
        'kota_nama' => 'required|unique:tb_kota,kota_nama'
      ];
    return $rules;
  }

  public function messages()
  {
    return [
        'kota_nama.required' => 'Bidang isian nama kota wajib diisi.',
    ];
  }

  public function create()
  {
    return view('kota.kota_create',[
      'JsValidator' => JsValidator::make($this->rulesCreate()),
      'negara' => Negara::all(),
    ]);
  }

  public function store(Request $request)
  {
    $data = $this->validate($request,$this->rulesCreate());
    $db = new Kota;
    $db->kota_nama = $request->kota_nama;
    $db->negara_id = $request->negara_id;

    if ($db->save()) {
      return redirect()->route('kota.index');
    }
  }

  public function edit($id)
  {
    return view('kota.kota_edit',[
      'data' => Kota::find($id),
      'negara' => Negara::all(),
    ]);
  }

  public function update(Request $request , $id)
  {
    $db = Kota::find($id);
    $db->kota_nama = $request->kota_nama;
    $db->negara_id = $request->negara_id;
    if($db->save()){
        return redirect()->route('kota.index');
    }
  }

  public function destroy($id)
  {
    Kota::destroy($id);
    return redirect()->route('kota.index');
  }
}
