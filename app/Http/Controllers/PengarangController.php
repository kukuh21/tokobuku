<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use App\Pengarang;
use JsValidator;
use Illuminate\Validation\Rule;

class PengarangController extends Controller
{
  public function index()
  {
    return view('pengarang.pengarang_index');
  }

  public function listData()
  {
    $pengarang = Pengarang::all();
    $no = 0;
    $data = array();
    foreach($pengarang as $list){
      $no ++;
      $row = array();
      $row[] = $no;
      $row[] = 'PG0'.$list->pengarang_id;
      $row[] = $list->pengarang_nama;
      $row[] = '<div class="btn-group">
              <a href="' . route('pengarang.edit', $list->pengarang_id) . '" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
              <a onclick="deleteData('.$list->pengarang_id.')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a></div>';
      $data[] = $row;
    }
    $output = array("data" => $data);
    return response()->json($output);
  }

  public function rulesCreate()
  {
    $rules = [
        'pengarang_nama' => 'required|unique:tb_pengarang,pengarang_nama'
      ];
    return $rules;
  }

  public function messages()
  {
    return [
        'pengarang_nama.required' => 'Bidang isian nama pengarang wajib diisi.',
    ];
  }

  public function create()
  {
    return view('pengarang.pengarang_create',[
      'JsValidator' => JsValidator::make($this->rulesCreate()),
    ]);
  }

  public function store(Request $request)
  {
    // $data = $this->validate($request,$this->rulesCreate());
    $db = new Pengarang;
    $db->pengarang_nama = $request->pengarang_nama;
    if ($db->save()) {
      return redirect()->route('pengarang.index');
    }
  }

  // public function rulesEdit($id)
  // {
  //   return [
  //     'nama_agama' => [
  //       'required',
  //       Rule::unique('tb_agama','agama_nama')->ignore($id,'agama_id'),
  //     ],
  //   ];
  // }
  //
  public function edit($id)
  {
    return view('pengarang.pengarang_edit',[
      // 'JsValidator' => JsValidator::make($this->rulesEdit($id)),
      'data' => Pengarang::find($id),
    ]);
  }

  public function update(Request $request , $id)
  {
    // $data = $this->validate($request,$this->rulesEdit($id));
    $db = Pengarang::find($id);
    $db->pengarang_nama = $request->pengarang_nama;
    if($db->save()){
        return redirect()->route('pengarang.index');
    }
  }

  public function destroy($id)
  {
    Pengarang::destroy($id);
    return redirect()->route('pengarang.index');
  }
}
