<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BuatTabelKota extends Migration
{
    public function up()
    {
      Schema::create('tb_kota', function(Blueprint $table){
          $table->increments('kota_id');
          $table->string('kota_kode', 255);
          $table->string('kota_nama', 255);
          $table->integer('negara_id');
          $table->timestamps();
      });
    }

    public function down()
    {
        Schema::drop('tb_kategori');
    }
}
