@extends('layouts.app')

@section('title')
  Tambah Penerbit
@endsection

@section('breadcrumb')
   @parent
   <li>Penerbit</li>
@endsection

@section('content')
<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-body">
        <form class="form-horizontal" id="form-input" method="post" action="{{ route('penerbit.update',$data->penerbit_id) }}">
          {{ csrf_field() }} {{ method_field('PUT') }}
          <div class="modal-body">
          <div class="form-group">
             <label for="nama" class="col-md-3 control-label">Nama Penerbit</label>
             <div class="col-md-6">
                <input id="penerbit_nama" type="text" class="form-control" name="penerbit_nama" required value="{{ $data->penerbit_nama }}">
                <span class="help-block with-errors"></span>
             </div>
          </div>
          <div class="form-group">
              <label class="control-label col-md-3">Kota</label>
              <div class="col-md-6">
                  <select class="form-control js-example-basic-single"
                          name="kota_id"
                          id="kota_id">
                      <option value="">
                          Pilih Kota
                      </option>
                      @foreach ($kota as $db)
                          <option value="{{ $db->kota_id }}"
                                  {{ $data->kota_id == $db->kota_id ?'selected' : '' }}>
                              {{ $db->kota_nama}}</option>
                      @endforeach
                  </select>
              </div>
          </div>
          </div>
          <div class="modal-footer">
             <button type="submit" class="btn btn-primary btn-save"><i class="fa fa-floppy-o"></i> Simpan </button>
             <button type="button" class="btn btn-warning" data-dismiss="modal"><i class="fa fa-arrow-circle-left"></i> Batal</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection

@section('script')

@endsection
