<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use App\Negara;
use JsValidator;
use Illuminate\Validation\Rule;

class NegaraController extends Controller
{
  public function index()
  {
    return view('negara.negara_index');
  }

  public function listData()
  {
    $negara = Negara::all();
    $no = 0;
    $data = array();
    foreach($negara as $list){
      $no ++;
      $row = array();
      $row[] = $no;
      $row[] = 'NG0'.$list->negara_id;
      $row[] = $list->negara_nama;
      $row[] = '<div class="btn-group">
              <a href="' . route('negara.edit', $list->negara_id) . '" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
              <a onclick="deleteData('.$list->negara_id.')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a></div>';
      $data[] = $row;
    }
    $output = array("data" => $data);
    return response()->json($output);
  }

  public function rulesCreate()
  {
    $rules = [
        'negara_nama' => 'required|unique:tb_negara,negara_nama'
      ];
    return $rules;
  }

  public function messages()
  {
    return [
        'negara_nama.required' => 'Bidang isian nama negara wajib diisi.',
    ];
  }

  public function create()
  {
    return view('negara.negara_create',[
      'JsValidator' => JsValidator::make($this->rulesCreate()),
    ]);
  }

  public function store(Request $request)
  {
    // $data = $this->validate($request,$this->rulesCreate());
    $db = new Negara;
    $db->negara_nama = $request->negara_nama;
    if ($db->save()) {
      return redirect()->route('negara.index');
    }
  }

  // public function rulesEdit($id)
  // {
  //   return [
  //     'nama_agama' => [
  //       'required',
  //       Rule::unique('tb_agama','agama_nama')->ignore($id,'agama_id'),
  //     ],
  //   ];
  // }
  //
  public function edit($id)
  {
    return view('negara.negara_edit',[
      // 'JsValidator' => JsValidator::make($this->rulesEdit($id)),
      'data' => Negara::find($id),
    ]);
  }

  public function update(Request $request , $id)
  {
    // $data = $this->validate($request,$this->rulesEdit($id));
    $db = Negara::find($id);
    $db->negara_nama = $request->negara_nama;
    if($db->save()){
        return redirect()->route('negara.index');
    }
  }

  public function destroy($id)
  {
    Negara::destroy($id);
    return redirect()->route('negara.index');
  }
}
