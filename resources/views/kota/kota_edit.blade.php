@extends('layouts.app')

@section('title')
  Tambah Data Kota
@endsection

@section('breadcrumb')
   @parent
   <li>Kota</li>
@endsection

@section('content')
<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-body">
        <form class="form-horizontal" id="form-input" method="post" action="{{ route('kota.store') }}">
          {{ csrf_field() }} {{ method_field('POST') }}
          <div class="modal-body">
          <div class="form-group">
             <label for="nama" class="col-md-3 control-label">Kota</label>
             <div class="col-md-6">
                <input id="negara_kode" type="text" value="{{ $data->kota_nama }}" class="form-control" name="negara_kode" required>
                <span class="help-block with-errors"></span>
             </div>
          </div>
          <div class="form-group">
              <label class="control-label col-md-3">Negara</label>
              <div class="col-md-6">
                  <select class="form-control js-example-basic-single"
                          name="negara_id"
                          id="negara_id">
                      <option value="">
                          Pilih Negara
                      </option>
                      @foreach ($negara as $db)
                          <option value="{{ $db->negara_id }}"
                                  {{ $data->negara_id == $db->negara_id ?'selected' : '' }}>
                              {{ $db->negara_nama}}</option>
                      @endforeach
                  </select>
              </div>
          </div>
          </div>
          <div class="modal-footer">
             <button type="submit" class="btn btn-primary btn-save"><i class="fa fa-floppy-o"></i> Simpan </button>
             <a href="{{ route('kota.index') }}" class="btn btn-warning"><i class="fa fa-arrow-circle-left"></i> Batal</a>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection

@section('script')

@endsection
