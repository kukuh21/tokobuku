<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DataTables;
use App\Kategori;
use JsValidator;
use Illuminate\Validation\Rule;

class KategoriController extends Controller
{
    public function index()
    {
      return view('kategori.kategori_index');
    }

    public function listData()
    {
      $kategori = Kategori::all();
      $no = 0;
      $data = array();
      foreach($kategori as $list){
        $no ++;
        $row = array();
        $row[] = $no;
        $row[] = 'KT0'.$list->kategori_id;
        $row[] = $list->kategori_nama;
        $row[] = '<div class="btn-group">
                <a href="' . route('kategori.edit', $list->kategori_id) . '" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                <a onclick="deleteData('.$list->kategori_id.')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a></div>';
        $data[] = $row;
      }
      $output = array("data" => $data);
      return response()->json($output);
    }

    public function rulesCreate()
    {
      $rules = [
          'kategori_nama' => 'required|unique:tb_kategori,kategori_nama'
        ];
      return $rules;
    }

    public function messages()
    {
      return [
          'kategori_nama.required' => 'Bidang isian nama kategori wajib diisi.',
      ];
    }

    public function create()
    {
      return view('kategori.kategori_create',[
        'JsValidator' => JsValidator::make($this->rulesCreate()),
      ]);
    }

    public function store(Request $request)
    {
      $data = $this->validate($request,$this->rulesCreate());
      $db = new Kategori;
      $db->kategori_nama = $request->kategori_nama;
      if ($db->save()) {
        return redirect()->route('kategori.index');
      }
    }

    // public function rulesEdit($id)
    // {
    //   return [
    //     'nama_agama' => [
    //       'required',
    //       Rule::unique('tb_agama','agama_nama')->ignore($id,'agama_id'),
    //     ],
    //   ];
    // }
    //
    public function edit($id)
    {
      return view('kategori.kategori_edit',[
        // 'JsValidator' => JsValidator::make($this->rulesEdit($id)),
        'data' => Kategori::find($id),
      ]);
    }

    public function update(Request $request , $id)
    {
      // $data = $this->validate($request,$this->rulesEdit($id));
      $db = Kategori::find($id);
      $db->kategori_nama = $request->kategori_nama;
      if($db->save()){
          return redirect()->route('kategori.index');
      }
    }

    public function destroy($id)
    {
      Kategori::destroy($id);
      return redirect()->route('kategori.index');
    }
}
