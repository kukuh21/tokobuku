@extends('layouts.app_utana')

@section('title')
  Daftar Buku
@endsection

@section('breadcrumb')
   @parent
   <li>Buku</li>
@endsection

@section('content')
<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-body">
      <form action="{{ url()->current() }}">
        <div class="col-md-12">
          <input type="text" name="keyword" class="form-control" placeholder="Search name...">
        </div>
        <br><br>
        <div class="col-md-12">
          <button type="submit" class="btn btn-primary">
              Search
          </button>
        </div>
        <br><br>
        @foreach ($buku as $db)
        <div class="col-md-4">
          <div class="panel panel-info">
            <div class="panel-body">
              <center><b>{{ $db->buku_nama }}</b></center>
              <hr>
              <div class="col-md-4">
                <img width="80"
                 src="{{ asset('public/images/buku.jpg') }}"/>
              </div>
              <div class="col-md-8">
                Kategori  : {{ $db->kategori_nama }}<br>
                Pengarang :{{ $db->pengarang_nama }}<br>
                Penerbit  :{{ $db->penerbit_nama }}<br>
              </div>
            </div>
            <div class="panel-footer" style="height: 40px;">
                  <a style="float: left; color: green;">Harga : {{ $db->buku_harga }}</a>
                  <a style="float: right; color: red;">Stok : {{ $db->buku_stok }}</a>
            </div>
          </div>
        </div>
        @endforeach

      </form>
      </div>
    </div>
  </div>
</div>
@endsection

@section('script')
<script>
    $(function() {
        table = $('.table').DataTable({
            "processing" : true,
            "ajax" : {
            "url" : "{{ route('homeutama.data') }}",
            "type" : "GET"
            }
        });
    });
</script>
@endsection
