@extends('layouts.app')

@section('title')
  Tambah Data Kategori
@endsection

@section('breadcrumb')
   @parent
   <li>Kategori</li>
@endsection

@section('content')
<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-body">
        <form class="form-horizontal" id="form-input" method="post" action="{{ route('kategori.update',$data->kategori_id) }}">
          {{ csrf_field() }} {{ method_field('PUT') }}
          <div class="modal-body">
          <div class="form-group">
             <label for="nama" class="col-md-3 control-label">Nama Kategori</label>
             <div class="col-md-6">
                <input id="kategori_nama" type="text" class="form-control" name="kategori_nama" value="{{ $data->kategori_nama }}" required>
                <span class="help-block with-errors"></span>
             </div>
          </div>
          </div>
          <div class="modal-footer">
             <button type="submit" class="btn btn-primary btn-save"><i class="fa fa-floppy-o"></i> Simpan </button>
             <a href="{{ route('kategori.index') }}" class="btn btn-warning"><i class="fa fa-arrow-circle-left"></i> Batal</a>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection

@section('script')

@endsection
