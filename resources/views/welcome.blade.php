<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <title>Toko Buku</title>
//
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <link rel="stylesheet" href="{{ asset('public/adminLTE/bootstrap/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('public/adminLTE/dist/css/AdminLTE.min.css') }}">
  <link rel="stylesheet" href="{{ asset('public/adminLTE/plugins/iCheck/square/blue.css') }}">
</head>

<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    Aplikasi Toko Buku
  </div>
  <div class="login-box-body">
    <p class="login-box-msg">Silakan Pilih Tombol Dibawah</p>
    <center>
    <a href="{{ route('bukuutama') }}" class="btn btn-success"><i class="fa fa-plus-circle"></i> Data Buku</a>
    <a href="{{ route('homepegawai') }}" class="btn btn-success"><i class="fa fa-plus-circle"></i> Pegawai</a>
    </center>
  </div>
</div>

<script src="{{ asset('public/adminLTE/plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
<script src="{{ asset('public/adminLTE/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('public/adminLTE/plugins/iCheck/icheck.min.js') }}"></script>
</body>
</html>
