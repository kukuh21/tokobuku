 <?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', 'HomeController@index')->name('home');

// Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/pegawai', 'HomePegawaiController@index')->name('homepegawai');
Route::get('/homeutama', 'HomeUtamaController@index')->name('homeutama');
Route::get('/bukuutama', 'HomeUtamaController@buku')->name('bukuutama');
Route::get('/homeutama/data', 'HomeUtamaController@listdata')->name('homeutama.data');


Route::get('/kategori/data', 'KategoriController@listdata')->name('kategori.data');
    Route::resource('/kategori','KategoriController',[
        'except' => 'show'
]);

Route::get('/pengarang/data', 'PengarangController@listdata')->name('pengarang.data');
    Route::resource('/pengarang','PengarangController',[
        'except' => 'show'
]);

Route::get('/negara/data', 'NegaraController@listdata')->name('negara.data');
    Route::resource('/negara','NegaraController',[
        'except' => 'show'
]);

Route::get('/kota/data', 'KotaController@listdata')->name('kota.data');
    Route::resource('/kota','KotaController',[
        'except' => 'show'
]);

Route::get('/penerbit/data', 'PenerbitController@listdata')->name('penerbit.data');
    Route::resource('/penerbit','PenerbitController',[
        'except' => 'show'
]);

Route::get('/buku/data', 'BukuController@listdata')->name('buku.data');
    Route::resource('/buku','BukuController',[
        'except' => 'show'
]);
