<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BuatTabelKat extends Migration
{
    public function up()
    {
      Schema::create('tb_kategori', function(Blueprint $table){
          $table->increments('kategori_id');
          $table->string('kategori_kode', 255);
          $table->string('kategori_nama', 255);
          $table->timestamps();
      });
    }

    public function down()
    {
        Schema::drop('tb_kategori');
    }
}
