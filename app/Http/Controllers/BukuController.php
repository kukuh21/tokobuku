<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use App\Kategori;
use App\Penerbit;
use App\Pengarang;
use App\Buku;
use JsValidator;
use Illuminate\Validation\Rule;
use DB;

class BukuController extends Controller
{
  public function index()
  {
    return view('buku.buku_index');
  }

  public function listData()
  {
    $buku = DB::table('tb_buku')
              ->leftJoin('tb_kategori', 'tb_buku.kategori_id', '=', 'tb_kategori.kategori_id')
              ->leftJoin('tb_pengarang', 'tb_buku.pengarang_id', '=', 'tb_pengarang.pengarang_id')
              ->leftJoin('tb_penerbit', 'tb_buku.penerbit_id', '=', 'tb_penerbit.penerbit_id')
              ->get();
    $no = 0;
    $data = array();
    foreach($buku as $list){
      $no ++;
      $row = array();
      $row[] = $no;
      $row[] = 'BK00'.$list->buku_id;
      $row[] = $list->kategori_nama;
      $row[] = $list->buku_nama;
      $row[] = $list->pengarang_nama;
      $row[] = $list->buku_harga;
      $row[] = $list->buku_stok;
      $row[] = $list->penerbit_nama;

      $row[] = '<div class="btn-group">
              <a href="' . route('buku.edit', $list->buku_id) . '" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
              <a onclick="deleteData('.$list->buku_id.')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a></div>';
      $data[] = $row;
    }
    $output = array("data" => $data);
    return response()->json($output);
  }

  public function create()
  {
    return view('buku.buku_create',[
      'kategori' => Kategori::all(),
      'pengarang' => Pengarang::all(),
      'penerbit' => Penerbit::all(),
    ]);
  }

  public function store(Request $request)
  {
    $db = new Buku;
    $db->kategori_id = $request->kategori_id;
    $db->buku_nama =  $request->buku_nama;
    $db->pengarang_id =  $request->pengarang_id;
    $db->buku_harga =  $request->buku_harga;
    $db->buku_stok =  $request->buku_stok;
    $db->penerbit_id =  $request->penerbit_id;
    if ($db->save()) {
      return redirect()->route('buku.index');
    }
  }

  public function edit($id)
  {
    return view('buku.buku_edit',[
      'data' => Buku::find($id),
      'kategori' => Kategori::all(),
      'pengarang' => Pengarang::all(),
      'penerbit' => Penerbit::all(),
    ]);
  }

  public function update(Request $request , $id)
  {
    $db = Buku::find($id);
    $db->kategori_id = $request->kategori_id;
    $db->buku_nama =  $request->buku_nama;
    $db->pengarang_id =  $request->pengarang_id;
    $db->buku_harga =  $request->buku_harga;
    $db->buku_stok =  $request->buku_stok;
    $db->penerbit_id =  $request->penerbit_id;
    if($db->save()){
        return redirect()->route('buku.index');
    }
  }

  public function destroy($id)
  {
    Buku::destroy($id);
    return redirect()->route('buku.index');
  }
}
