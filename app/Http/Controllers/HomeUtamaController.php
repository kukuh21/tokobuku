<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use App\Kategori;
use App\Penerbit;
use App\Pengarang;
use App\Buku;
use JsValidator;
use Illuminate\Validation\Rule;
use DB;

class HomeUtamaController extends Controller
{
  public function buku(Request $request)
  {
    $buku = Buku::when($request->keyword, function ($query) use ($request) {
              $query->where('buku_nama', 'like', "%{$request->keyword}%");
              })
              ->leftJoin('tb_kategori', 'tb_buku.kategori_id', '=', 'tb_kategori.kategori_id')
              ->leftJoin('tb_pengarang', 'tb_buku.pengarang_id', '=', 'tb_pengarang.pengarang_id')
              ->leftJoin('tb_penerbit', 'tb_buku.penerbit_id', '=', 'tb_penerbit.penerbit_id')
              ->get();

    // $buku = DB::table('tb_buku')
    //           ->leftJoin('tb_kategori', 'tb_buku.kategori_id', '=', 'tb_kategori.kategori_id')
    //           ->leftJoin('tb_pengarang', 'tb_buku.pengarang_id', '=', 'tb_pengarang.pengarang_id')
    //           ->leftJoin('tb_penerbit', 'tb_buku.penerbit_id', '=', 'tb_penerbit.penerbit_id')
    //           ->get();
      // $buku = Buku::latest()->paginate(5);
      return view('home.buku_utama', compact('buku'));
  }

  public function index()
  {
    return view('home.utama');
  }

  public function listData()
  {
    $buku = DB::table('tb_buku')
              ->leftJoin('tb_kategori', 'tb_buku.kategori_id', '=', 'tb_kategori.kategori_id')
              ->leftJoin('tb_pengarang', 'tb_buku.pengarang_id', '=', 'tb_pengarang.pengarang_id')
              ->leftJoin('tb_penerbit', 'tb_buku.penerbit_id', '=', 'tb_penerbit.penerbit_id')
              ->get();
    $no = 0;
    $data = array();
    foreach($buku as $list){
      $no ++;
      $row = array();
      $row[] = $no;
      $row[] = 'BK00'.$list->buku_id;
      $row[] = $list->kategori_nama;
      $row[] = $list->buku_nama;
      $row[] = $list->pengarang_nama;
      $row[] = $list->buku_harga;
      $row[] = $list->buku_stok;
      $row[] = $list->penerbit_nama;
      $data[] = $row;
    }
    $output = array("data" => $data);
    return response()->json($output);
  }
}
