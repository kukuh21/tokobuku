<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use App\Penerbit;
use App\Kota;
use JsValidator;
use Illuminate\Validation\Rule;
use DB;
class PenerbitController extends Controller
{
    public function index()
    {
      return view('penerbit.penerbit_index');
    }

    public function listData()
    {
      $penerbit = DB::table('tb_penerbit')
                ->leftJoin('tb_kota', 'tb_penerbit.kota_id', '=', 'tb_kota.kota_id')
                ->leftJoin('tb_negara', 'tb_kota.negara_id', '=', 'tb_negara.negara_id')
                ->get();
      $no = 0;
      $data = array();
      foreach($penerbit as $list){
        $no ++;
        $row = array();
        $row[] = $no;
        $row[] = 'PN0'.$list->penerbit_id;
        $row[] = $list->penerbit_nama;
        $row[] = $list->kota_nama;
        $row[] = $list->negara_nama;
        $row[] = '<div class="btn-group">
                <a href="' . route('penerbit.edit', $list->penerbit_id) . '" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                <a onclick="deleteData('.$list->penerbit_id.')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a></div>';
        $data[] = $row;
      }
      $output = array("data" => $data);
      return response()->json($output);
    }

    public function create()
    {
      return view('penerbit.penerbit_create',[
        'kota' => Kota::all(),
      ]);
    }

    public function store(Request $request)
    {
      // $data = $this->validate($request,$this->rulesCreate());
      $db = new Penerbit;
      $db->penerbit_nama = $request->penerbit_nama;
      $db->kota_id =  $request->kota_id;
      if ($db->save()) {
        return redirect()->route('penerbit.index');
      }
    }

    public function edit($id)
    {
      return view('penerbit.penerbit_edit',[
        // 'JsValidator' => JsValidator::make($this->rulesEdit($id)),
        'data' => Penerbit::find($id),
        'kota' => Kota::all(),
      ]);
    }

    public function update(Request $request , $id)
    {
      // $data = $this->validate($request,$this->rulesEdit($id));
      $db = Penerbit::find($id);
      $db->penerbit_nama = $request->penerbit_nama;
      $db->kota_id =  $request->kota_id;
      if($db->save()){
          return redirect()->route('penerbit.index');
      }
    }

    public function destroy($id)
    {
      Penerbit::destroy($id);
      return redirect()->route('penerbit.index');
    }


}
