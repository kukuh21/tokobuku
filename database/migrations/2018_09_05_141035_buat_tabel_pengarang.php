<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BuatTabelPengarang extends Migration
{
    public function up()
    {
      Schema::create('tb_pengarang', function(Blueprint $table){
          $table->increments('pengarang_id');
          $table->string('pengarang_kode', 255);
          $table->string('pengarang_nama', 255);
          $table->timestamps();
      });
    }

    public function down()
    {
        Schema::drop('tb_kategori');
    }
}
