<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pengarang extends Model
{
  protected $table = 'tb_pengarang';
  protected $primaryKey = 'pengarang_id';
}
