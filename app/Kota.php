<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Kota extends Model
{
  protected $table = 'tb_kota';
  protected $primaryKey = 'kota_id';

  public function negara()
  {
    return $this->belongsTo('App\Negara','negara_id');
  }

}
