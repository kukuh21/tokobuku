<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Penerbit extends Model
{
      protected $table = 'tb_penerbit';
      protected $primaryKey = 'penerbit_id';

      public function kota()
      {
        return $this->belongsTo('App\Kota','kota_id');
      }
}
