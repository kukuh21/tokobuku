@extends('layouts.app')

@section('title')
  Tambah Buku
@endsection

@section('breadcrumb')
   @parent
   <li>Buku</li>
@endsection

@section('content')
<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-body">
        <form class="form-horizontal" id="form-input" method="post" action="{{ route('buku.store') }}">
          {{ csrf_field() }} {{ method_field('POST') }}
          <div class="modal-body">
          <div class="form-group">
              <label class="control-label col-md-3">Kategori</label>
              <div class="col-md-6">
                  <select class="form-control js-example-basic-single"
                          name="kategori_id"
                          id="kategori_id">
                      <option value="">
                          Pilih Kategori
                      </option>
                      @foreach ($kategori as $db)
                          <option value="{{ $db->kategori_id }}">{{ $db->kategori_nama}}</option>
                      @endforeach
                  </select>
              </div>
          </div>
          <div class="form-group">
             <label for="nama" class="col-md-3 control-label">Nama Buku</label>
             <div class="col-md-6">
                <input id="buku_nama" type="text" class="form-control" name="buku_nama" required>
                <span class="help-block with-errors"></span>
             </div>
          </div>
          <div class="form-group">
              <label class="control-label col-md-3">Pengarang</label>
              <div class="col-md-6">
                  <select class="form-control js-example-basic-single"
                          name="pengarang_id"
                          id="pengarang_id">
                      <option value="">
                          Pilih Pengarang
                      </option>
                      @foreach ($pengarang as $db)
                          <option value="{{ $db->pengarang_id }}">{{ $db->pengarang_nama}}</option>
                      @endforeach
                  </select>
              </div>
          </div>
          <div class="form-group">
             <label for="nama" class="col-md-3 control-label">Harga</label>
             <div class="col-md-6">
                <input id="buku_harga" type="number" class="form-control" name="buku_harga" required>
                <span class="help-block with-errors"></span>
             </div>
          </div>
          <div class="form-group">
             <label for="nama" class="col-md-3 control-label">Stok</label>
             <div class="col-md-6">
                <input id="buku_stok" type="number" class="form-control" name="buku_stok" required>
                <span class="help-block with-errors"></span>
             </div>
          </div>
          <div class="form-group">
              <label class="control-label col-md-3">Penerbit</label>
              <div class="col-md-6">
                  <select class="form-control js-example-basic-single"
                          name="penerbit_id"
                          id="penerbit_id">
                      <option value="">
                          Pilih Penerbit
                      </option>
                      @foreach ($penerbit as $db)
                          <option value="{{ $db->penerbit_id }}">{{ $db->penerbit_nama }}</option>
                      @endforeach
                  </select>
              </div>
          </div>
          </div>
          <div class="modal-footer">
             <button type="submit" class="btn btn-primary btn-save"><i class="fa fa-floppy-o"></i> Simpan </button>
             <button type="button" class="btn btn-warning" data-dismiss="modal"><i class="fa fa-arrow-circle-left"></i> Batal</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection

@section('script')

@endsection
