<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BuatTabelNegara extends Migration
{
    public function up()
    {
      Schema::create('tb_negara', function(Blueprint $table){
          $table->increments('negara_id');
          $table->string('negara_kode', 255);
          $table->string('negara_nama', 255);
          $table->timestamps();
      });
    }

    public function down()
    {
        Schema::drop('tb_kategori');
    }
}
