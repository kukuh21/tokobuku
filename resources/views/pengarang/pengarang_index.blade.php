@extends('layouts.app')

@section('title')
  Daftar Pengarang
@endsection

@section('breadcrumb')
   @parent
   <li>Pengarang</li>
@endsection

@section('content')
<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-header">
        <a href="{{ route('pengarang.create') }}" class="btn btn-success"><i class="fa fa-plus-circle"></i> Tambah</a>
      </div>
      <div class="box-body">

  <form method="post" id="form-produk">
  {{ csrf_field() }}
      <table class="table table-striped">
      <thead>
         <tr>
            <th width="20">No</th>
              <th>Kode Pengarang</th>
              <th>Nama Pengarang</th>
            <th width="100">Aksi</th>
         </tr>
      </thead>
      <tbody></tbody>
      </table>
      </form>
      </div>
    </div>
  </div>
</div>
@endsection

@section('script')
<script>
    $(function() {
        table = $('.table').DataTable({
            "processing" : true,
            "ajax" : {
            "url" : "{{ route('pengarang.data') }}",
            "type" : "GET"
            }
        });
    });

    function deleteData(id){
       if(confirm("Apakah yakin data akan dihapus?")){
         $.ajax({
           url : "pengarang/"+id,
           type : "POST",
           data : {'_method' : 'DELETE', '_token' : $('meta[name=csrf-token]').attr('content')},
           success : function(data){
             table.ajax.reload();
           },
           error : function(){
             alert("Tidak dapat menghapus data!");
           }
         });
       }
    }
</script>
@endsection
