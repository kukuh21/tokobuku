@extends('layouts.app_utana')

@section('title')
  Daftar Buku
@endsection

@section('breadcrumb')
   @parent
   <li>Buku</li>
@endsection

@section('content')
<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-body">

  <form method="post" id="form-produk">
  {{ csrf_field() }}
      <table class="table table-striped">
      <thead>
         <tr>
            <th width="20">No</th>
              <th>Kode Buku</th>
              <th>Kategori</th>
              <th>Nama Buku</th>
              <th>Pengarang Buku</th>
              <th>Harga</th>
              <th>Stok</th>
              <th>Penerbit</th>
         </tr>
      </thead>
      <tbody></tbody>
      </table>
      </form>
      </div>
    </div>
  </div>
</div>
@endsection

@section('script')
<script>
    $(function() {
        table = $('.table').DataTable({
            "processing" : true,
            "ajax" : {
            "url" : "{{ route('homeutama.data') }}",
            "type" : "GET"
            }
        });
    });
</script>
@endsection
